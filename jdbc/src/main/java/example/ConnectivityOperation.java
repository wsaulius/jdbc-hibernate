package example;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectivityOperation {

    final static public String DB_PROPERTIES = "/database.properties";
    final static public String BOOKS_PROPERTIES = "/books.properties";
    protected Properties props = new Properties();

    public ConnectivityOperation() {

            try (final InputStream stream = this.getClass().getResourceAsStream( DB_PROPERTIES ) ) {
                props.load(stream);

                System.out.println( props );

        } catch ( IOException e) {
            e.printStackTrace();
        }

    }

}
