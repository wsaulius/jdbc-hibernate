package v2ch4.ViewDB; /**
   @version 1.30 2004-08-05
   @author Cay Horstmann
*/

import java.net.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
   This program uses metadata to display arbitrary tables
   in a database.
*/
public class ViewDB
{  
   public static void main(String[] args)
   {  
      JFrame frame = new ViewDBFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
   }
}

