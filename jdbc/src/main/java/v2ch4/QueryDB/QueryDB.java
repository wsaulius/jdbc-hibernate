package v2ch4.QueryDB; /**
   @version 1.22 2004-08-06
   @author Cay Horstmann
*/

import java.net.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
   This program demonstrates several complex database queries.
*/
public class QueryDB
{  
   public static void main(String[] args)
   {  
      JFrame frame = new QueryDBFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
   }
}

