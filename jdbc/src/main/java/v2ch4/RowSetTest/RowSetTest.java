package v2ch4.RowSetTest; /**
   @version 1.00 2004-08-10
   @author Cay Horstmann
*/

import javax.sql.rowset.*;

import java.net.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.sql.*;
import javax.sql.rowset.*;

/**
   This program uses metadata to display arbitrary tables
   in a database.
*/
public class RowSetTest
{  
   public static void main(String[] args)
   {  
      JFrame frame = new RowSetFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
   }
}

