package donatas.exercises;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConnectivityOperation {

    final public static String HUMAN_DATABASE = "/humanResources.properties";

    Properties props = new Properties();

    public ConnectivityOperation(){
        try(final InputStream stream = this.getClass().getResourceAsStream(HUMAN_DATABASE)) {
            props.load(stream);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
