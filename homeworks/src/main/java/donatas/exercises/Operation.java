package donatas.exercises;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class Operation extends ConnectivityOperation {

    private static Connection conn = null;
    private static Statement stmt = null;
    private static ResultSet rs = null;

    public Operation() {

        super();

        String drivers = props.getProperty("jdbc.drivers");
        if (null != drivers) System.setProperty("jdbc.drivers", drivers);
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {

            List fragments = null;
            try {
                fragments = Arrays.asList( new URI(url).toASCIIString().split("/" ));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            // Replace with log.info
            System.out.println(
               String.format (
                "%nOpened connection to %s on %s DB schema %s%n",
                       fragments.get(0),
                       fragments.get(2),
                       fragments.get(3).toString()
                               .replaceAll("\\?.*","")
                               .toUpperCase() ));
        }
    }

    public static void queryOperation() throws SQLException {
        try {
            new Operation().ex_1(Commands.EX_1);
            System.out.println("==============================================================");
            new Operation().ex_2(Commands.EX_2);
            System.out.println("==============================================================");
            new Operation().ex_3(Commands.EX_3);
            System.out.println("==============================================================");
            new Operation().ex_4(Commands.EX_4);
            System.out.println("==============================================================");
            new Operation().ex_5(Commands.EX_5);

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (null != conn) {
                conn.close();
            }
        }
    }

    protected void ex_1(String exercise) throws SQLException {
        try {
            if (null != conn) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(exercise);

                while (rs.next()) {

                    Integer projectId = rs.getInt("projectId");
                    String description = rs.getString("description");

                    System.out.println("ROW: " + rs.getRow() + " |projectId: " + projectId + "| " + " |description: " + description + "|.");
                }

            } else throw new SQLException("Connection could not be initialized.");

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (null != stmt) {
                stmt.close();
            }
            if (null != rs) {
                rs.close();
            }
        }
    }

    protected void ex_2(String exercise) throws SQLException {
        try {
            if (null != conn) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(exercise);

                while (rs.next()) {

                    Integer employeeId = rs.getInt("employeeId");
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    Date dateOfBirth = rs.getDate("dateOfBirth");

                    System.out.println("ROW: " + rs.getRow() + " |employeeId: " + employeeId + "| " + " |firstName: " + firstName + "| " + " |lastName: " + lastName + "| " +
                            " |dateOfBirth: " + dateOfBirth + "|.");

                }
            } else throw new SQLException("Connection could not be initialized.");
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (null != stmt) {
                stmt.close();
            }
            if (null != rs) {
                rs.close();
            }
        }
    }

    protected void ex_3(String exercise) throws SQLException {
        try {
            if (null != conn) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(exercise);

                while (rs.next()) {

                    Integer employeeId = rs.getInt("employeeId");
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    Date dateOfBirth = rs.getDate("dateOfBirth");

                    System.out.println("ROW: " + rs.getRow() + " |employeeId: " + employeeId + "| " + " |firstName: " + firstName + "| " + " |lastName: " + lastName + "| " +
                            " |dateOfBirth: " + dateOfBirth + "|.");

                }
            } else throw new SQLException("Connection could not be initialized.");
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (null != stmt) {
                stmt.close();
            }
            if (null != rs) {
                rs.close();
            }
        }
    }

    protected void ex_4(String exercise) throws SQLException {
        try {
            if (null != conn) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(exercise);

                while (rs.next()) {

                    Integer employeeId = rs.getInt("employeeId");
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    Date dateOfBirth = rs.getDate("dateOfBirth");

                    System.out.println("ROW: " + rs.getRow() + " |employeeId: " + employeeId + "| " + " |firstName: " + firstName + "| " + " |lastName: " + lastName + "| " +
                            " |dateOfBirth: " + dateOfBirth + "|.");

                }
            } else throw new SQLException("Connection could not be initialized.");
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (null != stmt) {
                stmt.close();
            }
            if (null != rs) {
                rs.close();
            }
        }
    }

    protected void ex_5(String exercise) throws SQLException {
        try {
            if (null != conn) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(exercise);

                while (rs.next()) {

                    Integer employeeId = rs.getInt("employeeId");
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    Date dateOfBirth = rs.getDate("dateOfBirth");
                    String departmentName = rs.getString("Department");

                    System.out.println("ROW: " + rs.getRow() + " |employeeId: " + employeeId + "| " + " |firstName: " + firstName + "| " + " |lastName: " + lastName + "| " +
                            " |dateOfBirth: " + dateOfBirth + "| " + " |Department: " + departmentName + "|.");

                }
            } else throw new SQLException("Connection could not be initialized.");
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (null != stmt) {
                stmt.close();
            }
            if (null != rs) {
                rs.close();
            }
        }
    }
}
