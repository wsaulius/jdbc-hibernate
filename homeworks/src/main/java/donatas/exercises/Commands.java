package donatas.exercises;//        1. Display all projects (projectId, description)
//        2. Display all employees (employeeId, firstName, lastName, dateOfBirth)
//        3. Display all employees with names starting with the letter J (employeeId, firstName, lastName,
//        dateOfBirth)
//        4. Display all employees that haven’t been assigned to a department
//        5. Display all employees along with the department they’re in (employeeId, firstName, lastName,
//        dateOfBirth, departmentName)

public class Commands {

    public static String EX_1 = "SELECT projectId, description FROM projects";
    public static String EX_2 = "SELECT employeeId, firstName, lastName, dateOfBirth FROM employees";
    public static String EX_3 = "SELECT employeeId, firstName, lastName, dateOfBirth FROM employees WHERE firstName LIKE 'J%'";
    public static String EX_4 = "SELECT employeeId, firstName, lastName, dateOfBirth FROM employees WHERE departmentId IS NULL";
    public static String EX_5 = "SELECT employeeId, firstName, lastName, dateOfBirth, departments.name AS Department FROM employees JOIN departments ON employees.departmentId = departments.departmentId";



}
