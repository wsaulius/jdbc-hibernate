package donatas.exercises;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {

        try {
            Operation.queryOperation();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
