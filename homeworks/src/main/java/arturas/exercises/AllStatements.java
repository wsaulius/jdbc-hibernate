package arturas.exercises;

public class AllStatements {
    static final public String SELECT_ALL = "SELECT projectId, description AS project FROM projects";
    static final public String SELECT_ALL_FROM_EMPLOYEES = "SELECT employeeId, firstName, lastName, dateOfBirth FROM employees";
}
