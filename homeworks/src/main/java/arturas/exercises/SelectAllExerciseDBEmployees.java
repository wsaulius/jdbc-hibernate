package arturas.exercises;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class SelectAllExerciseDBEmployees extends ConnectivityOperation implements ISelectingOperation, AutoCloseable {

    private static final Logger logger = LogManager.getLogger(SelectAllExerciseDBEmployees.class);
    private ISelectingOperation operation;

    public AutoCloseable queryOperation() throws SQLException {
        try {

            operation = new SelectAllExerciseDBEmployees();

            operation.selectAll( );
            operation.selectAll("J");

        } catch (SQLException throwables) {

            throwables.printStackTrace();
            // log here : lo4j
            logger.error( throwables );
        }

        return this;
    }

    @Override
    public void selectAll( String ... params ) throws SQLException {

        // Check if params are present
        if ( params.length > 0 ) {

            this.selectAll( params[ 0] );
        } else {

        // Very JAVA7 here
        // ResultSet rs = null;
        // Statement statement = null;

        // Rewrite with Automatic Resource Management (ARM) in JDK8
        // it is called "The try-with-resources Statement"
        // https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html

        try ( final Statement statement = conn.createStatement();
              final ResultSet rs =  statement.executeQuery(AllStatements.SELECT_ALL_FROM_EMPLOYEES);
        ) {
            if (null != conn) {

                // Iterate through results
                while (rs.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf( rs.getLong("employeeId") );

                    String asFirstName = String.valueOf(rs.getString("firstName"));
                    String asLastName = String.valueOf(rs.getString("lastName"));
                    String asDateOfBirth = String.valueOf(rs.getDate("dateOfBirth"));

                    final Object firstNameObject = rs.getObject("firstnName");
                    final Object lastNameObject = rs.getObject("lastName");
                    final Object dateOfBirthObject = rs.getObject("dateOfBirth");

                    logger.info( "On row #" + rs.getRow() + " Employee ID " +
                            asBigInteger + ": " +
                                    firstNameObject.toString() + "|"+
                                    lastNameObject.toString() + "|"+
                                    dateOfBirthObject.toString()

                            );
                }

            } else {
                throw new SQLException("Connection could not be initialized.");
            }

        } catch (SQLException e) {
            // do NOT swallow excp

            logger.error( e );

        } finally {
            logger.warn( "But we can use finally anyway: closing SQL");
        } }
    }

    protected void selectAll(String letter) throws SQLException{
        ResultSet rs = null;
        Statement statement = null;
        try {

            if (null != conn) {

                logger.info("------------SELECT WHERE J employeeId, firstName, lastName, dateOfBirth -------------");
                statement = conn.createStatement();

                Map fieldsAndLetter = new HashMap<>();
                fieldsAndLetter.put( "firstName", "J" );
                fieldsAndLetter.put( "lastName", "K" );

                final String selectWithLetter =
                        String.format( AllStatements.SELECT_ALL_FROM_EMPLOYEES + " WHERE %s LIKE %s", "firstName",  "\'%"+ letter + "%\'");
                logger.info( selectWithLetter );

                rs = statement.executeQuery( selectWithLetter );

                // Iterate through results
                while (rs.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf( rs.getLong("employeeId") );
                    String asFirstName = String.valueOf(rs.getString("firstName"));
                    String asLastName = String.valueOf(rs.getString("lastName"));
                    String asDateOfBirth = String.valueOf(rs.getDate("dateOfBirth"));

                    final Object firstNameObject = rs.getObject("firstName");
                    final Object lastNameObject = rs.getObject("lastName");
                    final Object dateOfBirthObject = rs.getObject("dateOfBirth");
                    System.out.println( "On row #" + rs.getRow() + " Employee ID " +
                            asBigInteger + ": " +
                            firstNameObject.toString() + "|"+
                            lastNameObject.toString() + "|"+
                            dateOfBirthObject.toString()

                    );
                }

            } else {
                throw new SQLException("Connection could not be initialized.");
            }

        } catch (SQLException e) {

            // e.printStackTrace();
            // logger.error( e );
            throw e;

        } finally {

            if ( null != statement ) {
                statement.close();
            }
            if ( null != rs ) {
                rs.close();
            }
        }
    }

    @Override
    public void close() throws Exception {

        if (null != super.conn ) {
            conn.close();
            logger.warn( "OK, closed DB Connection in {}", this.getClass() );
        }

    }

}
