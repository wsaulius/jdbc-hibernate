package arturas.exercises;

import arturas.exercises.ConnectivityOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.sql.*;
import java.util.Properties;

public class SelectAllExerciseDBProjects extends ConnectivityOperation implements ISelectingOperation, AutoCloseable {

    private static final Logger logger = LogManager.getLogger(SelectAllExerciseDBProjects.class);

    // Constructor
    public SelectAllExerciseDBProjects() {

        super();

        final Properties props = super.loadProperties( new Properties() );

        String drivers = props.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);

        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        try {
            conn = DriverManager.getConnection(url, username, password);
            // SELECT * FROM ...
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    // Return interfacing for chaining
    public AutoCloseable queryOperation() throws SQLException {

        try {
            this.selectAll();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            // log here : lo4j

        }
        return this;
    }


    public void selectAll( String ... params ) throws SQLException {

        ResultSet rs = null;
        Statement statement = null;
        try {

            if (null != conn) {
                logger.warn("------------SELECT ALL projectId, description, lastName---------------------------");

                statement = conn.createStatement();
                logger.info( AllStatements.SELECT_ALL );

                rs = statement.executeQuery( AllStatements.SELECT_ALL );

                // Iterate through results
                while (rs.next()) {

                    BigInteger asBigInteger = BigInteger.valueOf( rs.getLong("projectId") );
                    final Object projectObject = rs.getObject("project");

                    System.out.println( "On row #" + rs.getRow() + " we fetch " +
                            asBigInteger + ": " + projectObject.toString() );
                }

            } else {
                throw new SQLException("Connection could not be initialized.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;

        } finally {

            if ( null != statement ) {
                statement.close();
            }
            if ( null != rs ) {
                rs.close();
            }
        }
    }

    @Override
    public void close() throws Exception {

        if (null != super.conn ) {
            conn.close();
            logger.warn( "OK, closed DB Connection in {}", this.getClass() );
        }

    }
}
