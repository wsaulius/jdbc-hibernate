package arturas.exercises;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

public class ConnectivityOperation implements Commands {

    final public static String HUMAN_DATABASE = "/humanResources.properties";

    private Properties props;
    protected static Connection conn;

    public ConnectivityOperation(){
        try {

            this.props = loadProperties( new Properties( ) );

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Properties loadProperties(Properties properties) {

        try ( final InputStream stream = this.getClass().getResourceAsStream(HUMAN_DATABASE) ) {
            properties.load(stream);
            this.props = properties;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return this.props;
    }
}
