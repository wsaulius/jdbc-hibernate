package arturas.exercises;

import java.sql.SQLException;

// Prepare for Spring 5 framework for Java (google it)
public interface ISelectingOperation {

    void selectAll( String ... params ) throws SQLException;
}
