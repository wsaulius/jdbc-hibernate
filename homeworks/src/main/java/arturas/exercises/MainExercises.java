package arturas.exercises;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class MainExercises {

    private static final Logger logger = LogManager.getLogger(MainExercises.class);

    public static void main(String[] args){

        // Refactor for ARM (Autocloseable)
        try (AutoCloseable ignored = new SelectAllExerciseDBProjects().queryOperation();
             AutoCloseable runOnce = new SelectAllExerciseDBEmployees().queryOperation()
        ) {
            // All good, just for syntax. But we can log here too!
            logger.info( "So exit happily now, with all the resources auto-closed." );
        }

        catch (SQLException sql) {

            sql.printStackTrace();
            logger.error( sql );

        } catch (Exception e) {

            e.printStackTrace();
            logger.error( e );
        } finally {

            logger.warn( "Finished");
        }

    }
}

