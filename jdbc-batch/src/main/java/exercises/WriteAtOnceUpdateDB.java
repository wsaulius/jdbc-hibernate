package exercises;

import com.mifmif.common.regex.Generex;
import example.ConnectivityOperation;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.math.BigInteger;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

public class WriteAtOnceUpdateDB extends ConnectivityOperation {

    private static final String GENEREX_RX = "Java Project vr[1-4]\\.([3-8]\\.|[2-7]{1,2})";
    static List recs;
    private static Connection conn;

    static {
        // For Update in loop: use lambda
        recs = new Generex(GENEREX_RX).getAllMatchedStrings().stream()
                .filter(on -> !on.endsWith("."))
                .collect(Collectors.toList());
    }

    // Constructor
    public WriteAtOnceUpdateDB() {

        super();

        String drivers = props.getProperty("jdbc.drivers");
        if (drivers != null) System.setProperty("jdbc.drivers", drivers);

        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        try {
            conn = DriverManager.getConnection(url, username, password);
            conn.setAutoCommit(false);
            // SELECT * FROM ...
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void queryOperation() throws SQLException {
        try {
            new WriteAtOnceUpdateDB().selectAll().insertAll().selectAll();

        } catch (SQLException e) {
            e.printStackTrace();
            // log here : lo4j

        } finally {

            if (null != conn) {
                conn.close();
            }

        }
    }

    WriteAtOnceUpdateDB selectAll() throws SQLException {

        // Java8 optionals
        Optional<ResultSet> rs = Optional.empty();
        Optional<Statement> statement = Optional.empty();
        try {

            if (null != conn) {

                statement = Optional.of ( conn.createStatement() );
                rs = Optional.of ( statement.get().executeQuery(AllStatements.SELECT_ALL) );

                // Iterate through results
                while ( rs.get().next() ) {

                    BigInteger asBigInteger = BigInteger.valueOf(rs.get().getLong("projectId"));
                    final Object projectObject = rs.get().getObject("project");

                    System.out.println("On row #" + rs.get().getRow() + " we fetch " +
                            asBigInteger + ": " + projectObject.toString());
                }

            } else {
                throw new SQLException("Connection could not be initialized.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;

        } finally {

            if (statement.isPresent()) {
                statement.get().close();
            }
            if (rs.isPresent()) {
                rs.get().close();
            }
        }

        return this;
    }

    protected WriteAtOnceUpdateDB insertAll() throws SQLException {

        Optional<ResultSet> rs = Optional.empty();
        Optional<Statement> statement = Optional.empty();
        try {

            if (null != conn) {

                // Do it with JdbcTemplate (alternatively)
                JdbcTemplate template = new JdbcTemplate(this.getDataSource());

                // define query arguments: one single digit that is 10
                Object[] params = { 10 };

                // define SQL types of the arguments
                int[] types = { Types.BIGINT };

                // SQL Update function
                int rows = template.update(AllStatements.DELETE_001, params, types);

                System.out.println(rows + " row(s) deleted.");

                PreparedStatement preparedStatement = conn.prepareStatement(AllStatements.UPDATE_001);

                preparedStatement.setInt(1, 10);
                preparedStatement.setString(2, "Unknown Java project");

                // execute the prepared statement
                int row = preparedStatement.executeUpdate();

                final Iterator ii = recs.stream().sorted().iterator();

                preparedStatement = conn.prepareStatement(AllStatements.UPDATE_LOOP);
                while (ii.hasNext()) {
                    // Batching updates
                    try {
                        preparedStatement.setString(1, (String) ii.next());
                        preparedStatement.setInt(2, 10);

                        // Important!
                        preparedStatement.addBatch();

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }

                    // Clarify what we are executing
                    System.out.println("About to EXEC: " + preparedStatement.toString());

                }
                try {
                    int[] batch = preparedStatement.executeBatch();
                    System.out.println("Updated: " + batch.length + " in a batch.");

                    // Commit once
                    conn.commit();

                } catch (SQLException sql) {

                    conn.rollback();
                    sql.printStackTrace();
                    throw sql;

                } finally {

                    try {
                        preparedStatement.close();
                    } catch ( NoSuchElementException e ) {
                        // OK. means closed
                    }

                }

            } else {
                throw new SQLException("Connection could not be initialized.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;

        } finally {

            try {
                statement.get().close();
            } catch ( NoSuchElementException e ) {
                // OK. means closed
            }

            try {
                rs.get().close();
            } catch ( NoSuchElementException e ) {
                // OK. means closed
            }

        }
        return this;
    }

    public final DriverManagerDataSource getDataSource() {

        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(super.props.getProperty("jdbc.drivers"));
        dataSource.setUrl(super.props.getProperty("jdbc.url"));
        dataSource.setUsername(super.props.getProperty("jdbc.username"));
        dataSource.setPassword(super.props.getProperty("jdbc.password"));

        return dataSource;
    }

    static public class AllStatements {

        static final public String SELECT_ALL = "SELECT projectId, description AS project FROM projects";
        static final public String DELETE_001 = "DELETE FROM projects WHERE projectId = ?";
        static final public String UPDATE_001 = " INSERT INTO projects (projectId, description) VALUES (?, ?)";
        static final public String UPDATE_LOOP = " UPDATE projects SET description=? WHERE projectId = ?";

    }
}