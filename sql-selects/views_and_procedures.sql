/* create view */
CREATE VIEW teachers_by_class
AS
SELECT persons.firstName,persons.lastName,classrooms.name AS classroom FROM modules
JOIN topics
ON modules.topicId = topics.topicId
JOIN classrooms
ON modules.classroomId = classrooms.classroomId
JOIN persons
ON persons.personId = modules.trainerId;
/* call view */
SELECT * FROM teachers_by_class;
/* create procedure */
delimiter //
CREATE PROCEDURE teachers_class (IN teacherName VARCHAR(45))
BEGIN
	SELECT * FROM teachers_by_class
	WHERE firstName = teacherName;
END//
delimiter ;
/* call procedure for teacher Pete */
CALL teachers_class ('Pete');
SELECT * ;
/* call procedure for teacher Mark */
CALL teachers_class ('Mark');
SELECT * ;





