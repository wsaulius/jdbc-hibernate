
SELECT m.movieId, m.name, s.startTime FROM movies m, schedules s WHERE m.movieId = s.movieId AND s.startTime between '2020-01-01' and '2020-01-02' 

# LEFT JOIN 
SELECT * FROM movies LEFT JOIN schedules ON movies.movieId = schedules.movieId WHERE scheduleId IS NULL;


# Preparation 
INSERT INTO `movies` VALUES (5,'Titanic Remastered: International','Action',150,'The digital masterpiece');

# Select not scheduled 
SELECT m.movieId, m.name FROM movies m WHERE m.movieId NOT IN ( SELECT m.movieId FROM movies m, schedules s WHERE m.movieId = s.movieId )

 
# List all movies reserved by John Johnson.  
SELECT m.movieId, m.name, s.startTime FROM movies m, schedules s, reservations r, clients c 
WHERE m.movieId = s.movieId AND 
 r.scheduleId = s.scheduleId AND 
 m.movieId = s.movieId AND c.clientId = r.clientId AND c.firstName = 'John' AND c.lastName = 'Johnson';
 
# List seats reserved 
SELECT m.movieId, m.name, s.startTime, rs.seatId FROM movies m, schedules s, reservations r, clients c, reservation_seat rs 
WHERE m.movieId = s.movieId AND 
 r.scheduleId = s.scheduleId AND 
  m.movieId = s.movieId AND 
  c.clientId = r.clientId AND 
  rs.reservationId = r.reservationId 
AND c.firstName = 'John' AND c.lastName = 'Johnson';

# List all seats with (row,numbers) for given client 
SELECT m.movieId, m.name, s.startTime, rs.seatId, seats.row, seats.number FROM movies m, schedules s, reservations r, 
clients c, reservation_seat rs, seats  
WHERE m.movieId = s.movieId AND 
 r.scheduleId = s.scheduleId AND 
  m.movieId = s.movieId AND 
  c.clientId = r.clientId AND 
  rs.reservationId = r.reservationId AND 
  seats.seatId = rs.seatId 
AND c.firstName = 'John' AND c.lastName = 'Johnson';


# List as a sequence of previous entry 
SELECT m.movieId, m.name, s.startTime, rs.seatId, seats.row, seats.number FROM movies m, schedules s, reservations r, 
clients c, reservation_seat rs, seats, 
( SELECT clientId from clients c WHERE c.firstName = 'John' AND c.lastName = 'Johnson' ) by_id 
WHERE m.movieId = s.movieId AND 
 r.scheduleId = s.scheduleId AND 
  m.movieId = s.movieId AND 
  c.clientId = r.clientId AND 
  rs.reservationId = r.reservationId AND 
  seats.seatId = rs.seatId 
AND by_id.clientId = r.clientId; 


# List all tickets for "Joker" 
SELECT ticketId, movies.name FROM tickets JOIN schedules ON tickets.scheduleId = schedules.scheduleId 
JOIN movies ON schedules.movieId = movies.movieId WHERE movies.name = 'Joker'; 


# List all tickets for "Joker" 
SELECT t.ticketId, t.categoryId, m.name, m.movieId, tc.type FROM tickets t, schedules s, movies m, ticketCategories tc WHERE 
t.categoryId = tc.ticketCategoryId AND 
t.scheduleId = s.scheduleId AND m.movieId = s.movieId AND m.name = 'Joker' ; 


# AS (inner JOIN) 
SELECT ticketId, movies.name, ticketCategories.type FROM tickets JOIN schedules ON tickets.scheduleId = 
schedules.scheduleId JOIN movies ON schedules.movieId = movies.movieId JOIN ticketCategories ON 
tickets.categoryId = ticketCategories.ticketCategoryId WHERE movies.name = 'Joker';

# Explicit inner JOIN 
SELECT ticketId, movies.name, ticketCategories.type FROM tickets INNER JOIN schedules ON tickets.scheduleId = 
schedules.scheduleId INNER JOIN movies ON schedules.movieId = movies.movieId INNER JOIN ticketCategories ON 
tickets.categoryId = ticketCategories.ticketCategoryId WHERE movies.name = 'Joker';

# Homework 
## 8) List the average number of tickets sold for each movie.
## 9) List the highest number of tickets sold for a movie.

# Count number of tickets sold for a movie
SELECT count(ticketId), movies.movieId, movies.name FROM tickets INNER JOIN schedules ON tickets.scheduleId =
schedules.scheduleId INNER JOIN movies ON schedules.movieId = movies.movieId GROUP BY movies.movieId, movies.name;tickets

# List the unreserved seats for Joker.
SELECT seats.seatId, movies.name FROM schedules JOIN movies ON schedules.movieId = movies.movieId 
JOIN seats LEFT JOIN reservation_seat ON seats.seatId = reservation_seat.seatId 
WHERE reservationSeatId IS NULL AND movies.name = 'Joker';


# Group by usage, note c.client id as second SELECT parameter. 
SELECT count(rs.seatId), c.clientId FROM movies m, schedules s, reservations r, 
clients c, reservation_seat rs, seats, 
( SELECT clientId from clients c ) by_id 
WHERE m.movieId = s.movieId AND 
 r.scheduleId = s.scheduleId AND 
  m.movieId = s.movieId AND 
  c.clientId = r.clientId AND 
  rs.reservationId = r.reservationId AND 
  seats.seatId = rs.seatId 
AND by_id.clientId = r.clientId
GROUP BY c.clientId 


