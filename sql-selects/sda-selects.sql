

# All Students 
 SELECT * FROM sda.persons WHERE isTrainer = 0 

# All trainers 
SELECT * FROM sda.persons WHERE isTrainer = 1; 


# All Students from team  'Python1Tallin' 
 SELECT persons.personId, persons.teamId, persons.lastName, teams.name FROM sda.persons, sda.teams WHERE isTrainer = 0 AND 
 teams.teamId = persons.teamId AND teams.name = 'Python1Tallin'; 
 
# Alternatively  
SELECT * FROM persons JOIN teams ON persons.teamId = teams.teamId WHERE isTrainer = 0 AND teams.name = 'Python1Tallin';


# (optional) count students in each of groups 
# All Students : SQL GOTCHAS : Do not mix up sum and count 
SELECT count(persons.personId), teams.name FROM sda.persons, sda.teams WHERE isTrainer = 0 AND 
teams.teamId = persons.teamId GROUP BY teams.name; 


# List all groups that had classes in location BucharestCowork. 
SELECT modules.teamId, classrooms.name, modules.startDate, modules.endDate FROM modules JOIN classrooms ON 
modules.classroomId = classrooms.classroomId JOIN teams ON modules.teamId = teams.teamId 
WHERE classrooms.name = 'BucharestCowork' AND modules.endDate BETWEEN modules.endDate AND CURRENT_TIMESTAMP; 
 

# List all groups that had classes in location Tallin Cozy Space in March 2020.
SELECT modules.teamId, classrooms.name, modules.startDate, modules.endDate FROM modules JOIN classrooms ON 
modules.classroomId = classrooms.classroomId JOIN teams ON modules.teamId = teams.teamId 
WHERE classrooms.address LIKE 'Tall%' AND modules.endDate BETWEEN '2020-03-01' AND '2020-04-01'


# List all students that already finished the SQL module.
SELECT persons.firstName, persons.lastName, modules.teamId, topics.name, modules.startDate, modules.endDate FROM modules JOIN topics ON 
modules.topicId = topics.topicId JOIN teams ON modules.teamId = teams.teamId JOIN persons ON 
persons.teamId = teams.teamId 
WHERE topics.name = 'SQL' AND modules.endDate BETWEEN modules.endDate AND CURRENT_TIMESTAMP; 


# List all students with 100% attendance rate. 
SELECT sum(attendances.isPresent = 1)/count(attendances.attendanceId), persons.personId as id, persons.email FROM persons 
JOIN attendances ON attendances.studentId = persons.personId
WHERE persons.isTrainer = 0 AND isPresent = 1 GROUP BY persons.personId;

# Select alternatively 
SELECT COUNT(*),persons.firstName,persons.lastName FROM attendances JOIN persons ON attendances.studentId = 
persons.personId WHERE isPresent = 1 GROUP BY personId;

# SELECT slackers 
SELECT * FROM 
( SELECT persons.personId, persons.email, count(*) as missed FROM attendances JOIN persons ON attendances.studentId = 
persons.personId WHERE isPresent = 0 GROUP BY personId, persons.email ) AS slackers ;

SELECT p.personId, p.email FROM persons p,
( SELECT persons.personId, persons.email, count(*) as missed FROM attendances JOIN persons 
ON attendances.studentId = persons.personId WHERE isPresent = 0 GROUP BY personId, persons.email ) slackers 
WHERE slackers.personId != p.personId AND p.isTrainer = 0; 


SELECT p.personId, p.email FROM persons p,
( SELECT persons.personId, persons.email, count(*) as missed FROM attendances JOIN persons 
ON attendances.studentId = persons.personId WHERE isPresent = 0 GROUP BY personId, persons.email ) slackers 
WHERE slackers.personId != p.personId AND p.isTrainer = 0; 

SELECT count(attendances.isPresent), studentId  FROM attendances WHERE attendances.isPresent = 0 GROUP BY studentId 


# List all trainers that teach "Java Fundamentals"
SELECT persons.personId, persons.email, modules.startDate, modules.endDate, topics.name 
FROM modules 
JOIN persons ON modules.trainerId = persons.personId 
JOIN topics ON topics.topicId = modules.topicId 
WHERE topics.name = 'Java Fundamentals' AND persons.isTrainer = 1; 

# Alternatively, list all trainers that teach "Java For Dummies" 
SELECT persons.firstName,persons.lastName FROM modules JOIN topics ON modules.topicId = topics.topicId 
JOIN persons ON persons.personId = modules.trainerId WHERE topics.name LIKE 'Java F%';

## List all trainers that teach at BucharestCowork location.
SELECT persons.firstName, persons.lastName, classrooms.name FROM modules JOIN topics ON modules.topicId = topics.topicId 
JOIN persons ON persons.personId = modules.trainerId JOIN classrooms ON modules.classroomId = classrooms.classroomId 
WHERE classrooms.address LIKE 'Bucharest%' OR classrooms.name LIKE 'Bucharest%'; 

# List all trainers that taught students with 100% attendance rate, providing Mark is not present 
SELECT persons.firstName,persons.lastName FROM attendances 
JOIN modules ON attendances.moduleId = modules.moduleId 
JOIN persons ON modules.trainerId = persons.personId AND persons.isTrainer = 1 GROUP BY trainerId;

# List the trainer that taught the highest number of modules (with names) 
SELECT count(*) AS teaches_modules, persons.personId, persons.email 
FROM modules 
JOIN persons ON modules.trainerId = persons.personId 
JOIN topics ON topics.topicId = modules.topicId 
WHERE persons.isTrainer = 1 GROUP BY persons.personId, persons.email ORDER BY teaches_modules DESC LIMIT 1; 

#Just trainer ids 
 SELECT COUNT(moduleId) AS counter,persons.firstName,persons.lastName FROM modules 
 JOIN persons ON modules.trainerId = persons.personId GROUP BY personId 
 ORDER BY counter DESC LIMIT 1;
 
# Use views 
CREATE VIEW slackers 
AS SELECT persons.personId, persons.email, count(*) as missed FROM attendances JOIN persons 
ON attendances.studentId = persons.personId WHERE isPresent = 0 GROUP BY personId, persons.email; 

# Select from views 
SELECT firstName, lastName from slackers, persons p 
WHERE slackers.personId != p.personId AND p.isTrainer = 0; 
 
 
