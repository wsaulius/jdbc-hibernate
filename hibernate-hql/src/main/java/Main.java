import example.util.EmployeeQueryUtil;
import example.util.PersonQueryUtil;

public class Main {

    public static void main(String[] args) {

        try ( final AutoCloseable personCloseable = new PersonQueryUtil();
              final AutoCloseable employeeCloseable = new EmployeeQueryUtil() ) {

        } catch ( Exception e ) {

            e.printStackTrace();

        } finally {

        }
    }
}
