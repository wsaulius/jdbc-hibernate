package example.statements;

final public class QueryStatementsSecured {

    // This is a class reference (Employee.class)
    static final public String SELECT_ALL_EMPLOYEE = "FROM Employee";
    static final public String SELECT_ALL_EMPLOYEE_ASCD = "FROM Employee ORDER BY lastName";
    static final public String SELECT_ALL_EMPLOYEE_DESC = "FROM Employee ORDER BY lastName DESC";

    static final public String SELECT_ALL_JOIN = "FROM Employee e, Departments d WHERE d.departmentId = e.departmentId";

}
