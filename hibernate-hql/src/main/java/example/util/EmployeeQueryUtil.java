package example.util;

import example.dao.EmployeeDao;
import example.entity.Employee;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class EmployeeQueryUtil implements AutoCloseable {

    // Just a demo code, not actual constructor
    public EmployeeQueryUtil() {

        EmployeeDao employeeDao = new EmployeeDao();
        List<Employee> allEmployees = employeeDao.getEmployees();

        System.out.printf( "-- DAO --%n" );

        employeeDao.getEmployeeByFirstLetter( 'J' )
                .stream().forEachOrdered(System.out::println);

        System.out.printf( "-- Predicates --%n" );

        System.out.printf( "-- ALL Employess --%n" );
        allEmployees.stream().forEach( System.out::println );

        final Predicate findAllJ = employee -> (( Employee )employee).getFirstName().startsWith( "J" );

        System.out.println( "--J--" );
        allEmployees.stream().filter( findAllJ ).forEach( System.out::println );

        Arrays.asList( new String [] { "J", "S" } ).forEach(letter -> {

                    final Pattern pattern = Pattern.compile(String.format("^%s.*", letter));

                    List<String> matchingEmployees = allEmployees.stream().map(Employee::getFirstName).
                            filter(pattern.asPredicate())
                            .collect(Collectors.toList());

                    System.out.printf( "-- %s -- (regexp)%n", letter );
                    matchingEmployees.stream().forEach(System.out::println);
                }
        );
    }

    @Override
    public void close() throws Exception {

        // Check the singleton before closing
        if ( null != HibernateUtil.getSessionFactory() )
            HibernateUtil.shutdown();
    }

}
