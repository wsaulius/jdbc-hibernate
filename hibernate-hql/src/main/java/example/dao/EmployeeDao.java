package example.dao;

import example.entity.Employee;
import example.statements.QueryStatementsSecured;
import example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDao {

    public void createEmployee(Employee person) {

        Transaction transaction = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(person);
            // commit transaction
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();

        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public void updateEmployee(Employee person) {

        Transaction transaction = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(person);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();

        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public void deleteEmployee(Employee person) {

        Transaction transaction = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.delete(person);
            // commit transaction
            transaction.commit();

        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();

        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public Employee getEmployee(Long id) {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Employee person = session.find(Employee.class, id);
            return person;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;

        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public List<Employee> getEmployees() {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Employee> employees = session.createQuery( QueryStatementsSecured.SELECT_ALL_EMPLOYEE_ASCD,
                    Employee.class).list();

            return employees;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;

        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public List<Employee> getEmployeeByFirstLetter(final Character character) {

        Session session = null;
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            Query<Employee> query = session.createNamedQuery("get_employee_by_lastName", Employee.class);
            query.setParameter("name", String.format( "%s%%", character ) );   // LIKE J%

            return query.getResultStream().collect(Collectors.toList());

        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        } finally {

            if ( null != session ) {
                session.close();
            }

        }
    }
}
