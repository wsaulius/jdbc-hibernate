package example.dao;

import example.entity.Person;
import example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PersonDao {

    public void createPerson(Person person) {

        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.save(person);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public void updatePerson(Person person){

        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.update(person);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public void deletePerson(Person person){

        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the person object
            session.delete(person);
            // commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public Person getPerson(Long id){

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Person person = session.find(Person.class, id);
            return person;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }

    public List<Person> getPersons() {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            List<Person> persons = session.createQuery("from Person", Person.class).list();
            return persons;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        } finally {

            if ( null != session ) {
                session.close();
            }
        }
    }


}
